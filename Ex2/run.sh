LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

if [ -f ./main_dyn.x ]
then
	./main_dyn.x

else #falls nicht existiert, compiliere ggf. was fehlt...
    	
	while true; do
	read -p "Executable nicht gefunden, soll compiliert werden, y/n ? " yn
		case $yn in
        		[Yy]* ) make -f Makefile all;break;;
        		[Nn]* ) exit;;
        		* ) echo "Bitte mit y oder n antworten...";;
    		esac
	done

	./main_dyn.x
fi
