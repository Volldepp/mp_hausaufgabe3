LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

if [ -f ./main_dyn.x ] && [ -f ./main_stat.x ]
then
	./main_dyn.x
	./main_stat.x
else #falls eine der Beiden nicht existiert, compiliere was fehlt...
    	
	while true; do
	read -p "Beide/eine der executables nicht gefunden, soll compiliert werden, y/n ? " yn
		case $yn in
        		[Yy]* ) make -f Makefile all;break;;
        		[Nn]* ) exit;;
        		* ) echo "Bitte mit y oder n antworten...";;
    		esac
	done

	./main_dyn.x
	./main_stat.x
fi
